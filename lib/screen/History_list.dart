import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:widgetlifecycle/models/exmd.dart';

class list extends StatefulWidget {
  const list({Key? key}) : super(key: key);

  @override
  State<list> createState() => _listState();
}

class _listState extends State<list> {
  var date = "ເລືອກວັນທີ່";
  var items = [
    {
      "Name": "\nKhemthid",
      "From": "ຝາກໃຫ້",
      "Tell": "ເບີໂທ 02053547453",
      "Lekbin": "\nເລກໃບບິນ ZXXZSJIDJS",
      "SAKHA": "ສາຂາ ປາກເຊ",
      "fark": "D",
      "colorr" : Color.fromARGB(255, 250, 114, 2)
      
    },
    {
      "Name": "\nKhone",
      "From": "ຝາກມາຈາກ",
      "Tell": "ເບີໂທ 02056400555",
      "Lekbin": "\nເລກໃບບິນ MJSJFFF",
      "SAKHA": "ສາຂາ ປາກເຊ",
      "fark": "R",
      "colorr" : Color.fromARGB(255, 82, 81, 81)

    },
    {
      "Name": "\nSingto",
      "From": "ຝາກມາຈາກ",
      "Tell": "ເບີໂທ 02055174999",
      "Lekbin": "\nເລກໃບບິນ MXIJSIJDD",
      "SAKHA": "ສາຂາ ປາກເຊ",
      "fark": "R",
      "colorr" : Color.fromARGB(255, 82, 81, 81)
    },
    {
      "Name": "\nAtz",
      "From": "ຝາກໃຫ້",
      "Tell": "ເບີໂທ 02055174999",
      "Lekbin": "\nເລກໃບບິນ MXIJSIJDD",
      "SAKHA": "ສາຂາ ປາກເຊ",
      "fark": "D",
      "colorr" : Color.fromARGB(255, 250, 114, 2)
    },
    {
      "Name": "\nthit",
      "From": "ຝາກມາຈາກ",
      "Tell": "ເບີໂທ 02055174999",
      "Lekbin": "\nເລກໃບບິນ MXIJSIJDD",
      "SAKHA": "ສາຂາ ປາກເຊ",
      "fark": "R",
      "colorr" : Color.fromARGB(255, 82, 81, 81)
    },
    {
      "Name": "\nbeat",
      "From": "ຝາກໃຫ້",
      "Tell": "ເບີໂທ 02055174999",
      "Lekbin": "\nເລກໃບບິນ MXIJSIJDD",
      "SAKHA": "ສາຂາ ປາກເຊ",
      "fark": "D",
      "colorr" : Color.fromARGB(255, 250, 114, 2)
    },
    {
      "Name": "\nluk",
      "From": "ຝາກໃຫ້",
      "Tell": "ເບີໂທ 02055174999",
      "Lekbin": "\nເລກໃບບິນ MXIJSIJDD",
      "SAKHA": "ສາຂາ ປາກເຊ",
      "fark": "D",
      "colorr" : Color.fromARGB(255, 250, 114, 2)
    },
    {
      "Name": "\nky",
      "From": "ຝາກໃຫ້",
      "Tell": "ເບີໂທ 02055174999",
      "Lekbin": "\nເລກໃບບິນ MXIJSIJDD",
      "SAKHA": "ສາຂາ ປາກເຊ",
      "fark": "D",
      "colorr" : Color.fromARGB(255, 250, 114, 2)
    },
    {
      "Name": "\nbest",
      "From": "ຝາກມາຈາກ",
      "Tell": "ເບີໂທ 02055174999",
      "Lekbin": "\nເລກໃບບິນ MXIJSIJDD",
      "SAKHA": "ສາຂາ ປາກເຊ",
      "fark": "R",
      "colorr" : Color.fromARGB(255, 82, 81, 81)
    },
  ];

  List<Nani> Test = [];

  @override
  Widget build(BuildContext context) {
    for (var e in items) {
      var model = Nani.fromJson(e);
      Test.add(model);
    }
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        body: Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: AssetImage("image/bg.webp"),
              fit: BoxFit.fill,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 230),
            child: Container(
              height: double.infinity,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15)),
                color: Color.fromARGB(255, 255, 255, 255),
              ),
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.all(15),
                    alignment: Alignment.topCenter,
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10),
                                child: Text(
                                  "ລາຍການຝາກ",
                                  style: GoogleFonts.notoSansLao(color: Colors.orange) ,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10),
                                child: Text(
                                  "ລາຍການຮັບ",
                                  style: GoogleFonts.notoSansLao(),
                                ),
                              ),
                            ],
                          ),
                          TextButton(
                            style: ButtonStyle(
                                foregroundColor: MaterialStateProperty.all(
                                    Color.fromARGB(255, 10, 10, 10)),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                        side: BorderSide(
                                            color: Color.fromARGB(
                                                255, 251, 127, 11))))),
                            onPressed: () {
                              showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime.parse('2010-01-01'),
                                      lastDate: DateTime.parse('2030-01-01'))
                                  .then((value) {
                                //Todo: handle date to string
                                //print(DateFormat.yMMMd().format(value!));
                                final DateFormat formatter = DateFormat('MMM');
                                final String formatted =
                                    formatter.format(value!);
                                print(formatted); // something like 2013-04-20
                                // var tdate = value.toString().split(' ');
                                // print(tdate[0]);
                                setState(() {
                                  date = formatted;
                                });
                              });
                            },
                            child: Text(date),
                          ),
                        ]),
                  ),
                  Expanded(
                    child: ListView.separated(
                        separatorBuilder: ((context, index) => const Divider(
                              color: Color.fromARGB(255, 204, 202, 202),
                              // width: 100,
                              indent: 15,
                              endIndent: 15,
                              thickness: 1,
                            )),
                        itemCount: Test.length,
                        padding: EdgeInsets.zero,
                        itemBuilder: (_, i) {
                          return ListTile(
                            leading: Container(
                                height: 50,
                                width: 50,
                                decoration: BoxDecoration(
                                    color: Test[i].colorr,
                                    borderRadius: BorderRadius.circular(50)),
                                child: Center(
                                  child: Text(
                                    Test[i].fark!,
                                    style: TextStyle(
                                      fontSize: 32,
                                      color: Colors.white,
                                    ),
                                  ),
                                )),
                            trailing: Column(
                              children: [
                                Text(
                                  Test[i].sAKHA!,
                                  style: GoogleFonts.notoSansLao(
                                    color: Test[i].colorr!,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                            title: Text.rich(TextSpan(children: [
                              TextSpan(
                                text: Test[i].from!,
                                style: GoogleFonts.notoSansLao(fontSize: 14),
                              ),
                              TextSpan(
                                text: Test[i].name!,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ])),
                            // Text("ຝາກໃຫ້  \n Hip //$index")
                            subtitle: Text.rich(TextSpan(children: [
                              TextSpan(
                                text: Test[i].tell!,
                                style: GoogleFonts.notoSansLao(),
                              ),
                              TextSpan(
                                text: Test[i].lekbin!,
                                style: GoogleFonts.notoSansLao(),
                              ),
                            ])),
                          );
                        }),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
