import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:widgetlifecycle/screen/Home_wiget/IconBTN.dart';
import 'package:widgetlifecycle/screen/Home_wiget/NormalText.dart';
import 'package:widgetlifecycle/screen/Home_wiget/TextIcon.dart';


class Myhome extends StatefulWidget {
  const Myhome({Key? key}) : super(key: key);

  @override
  State<Myhome> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<Myhome> {
  int activeInDex = 0;
  final List<String> imgList = [
    'image/img1.jpg',
    'image/img2.jpg',
    'image/img3.jpg',
    'image/img4.jpg',
    
  ];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Container(
        decoration:  BoxDecoration(
          image:  DecorationImage(
            image: AssetImage("image/bg.webp"),
            fit: BoxFit.fill,
          ),
        ),
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          appBar: PreferredSize(
              child: AppBar(
                title: Text(
                  "ມະຫາຊົນ ເອັກຊເພຼສ",
                  style: GoogleFonts.notoSansLao(fontSize: 16),
                ),
                leading: Icon(
                  Icons.account_circle,
                  size: 50,
                ),
                actions: [
                  Icon(
                    Icons.notifications_none_rounded,
                  )
                ],
                backgroundColor: Colors.transparent,
                elevation: 0,
                flexibleSpace: SafeArea(
                    child: Column(
                  children: [
                    SizedBox(
                      height: 23,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 63,
                        ),
                        TextButton(
                          onPressed: () {},
                          child: Text(
                            "ເຂົ້າສູ່ລະບົບ >",
                            style: GoogleFonts.notoSansLao(
                                fontSize: 12,
                                color: Color.fromARGB(255, 230, 228, 228)),
                            textAlign: TextAlign.center,
                          ),
                        )
                      ],
                    )
                  ],
                )),
              ),
              preferredSize: Size.fromHeight(73)),
          body: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 11),
                      height: 25,
                      width: 5,
                      color: Colors.yellow,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    NormalText(text: "ຕິດຕາມເຄື່ອງ",),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Container(
                  height: 1,
                  width: double.infinity,
                  margin: EdgeInsets.only(left: 12, right: 12),
                  color: Colors.white,
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 0, right: 0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(10)),
                        height: 40,
                        width: 275,
                        margin: EdgeInsets.only(left: 20, right: 0),
                        child: TextField(
                          decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.search,
                                size: 35,
                              ),
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  width: 0,
                                  style: BorderStyle.none,
                                ),
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(10.0),
                                ),
                              ),
                              filled: true,
                              hintStyle: GoogleFonts.notoSansLao(
                                  color: Color.fromARGB(255, 115, 115, 115),
                                  height: 3.8,
                                  fontSize: 13),
                              hintText: "ປ້ອນໝາຍເລກຕິດຕາມເຄື່ອງ"),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(right: 0, left: 10, bottom: 0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 76, 75, 75),
                            borderRadius: BorderRadius.circular(10)),
                        height: 45,
                        width: 45,
                        child: IconButton(
                          icon: const Icon(Icons.fit_screen),
                          color: Colors.white,
                          onPressed: () {},
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 5,),
                Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0)),
                  height: 150,
                  width: 350,
                  
                  // color: Colors.red,
                  child: CarouselSlider(
                    options: CarouselOptions(
                      viewportFraction: 1,
                      aspectRatio: 2.0,
                      // enlargeCenterPage: true,
                      scrollDirection: Axis.horizontal,
                      autoPlay: true,
                      onPageChanged: (index, reason) =>
                          setState(() => activeInDex = index),
                    ),
                    items: imgList
                        .map((item) => Container(
                              child: Container(
                                margin: EdgeInsets.only(
                                    top: 10, bottom: 3, left: 0, right: 0),
                                child: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10.0)),
                                    child: Stack(
                                      children: <Widget>[
                                        Image.asset(item,
                                            fit: BoxFit.cover, width: 1000.0),
                                        Positioned(
                                          bottom: 5,
                                          left: 150,
                                          right: 0,
                                          child: buildIndicator(),
                                        )
                                      ],
                                    )),
                              ),
                            ))
                        .toList(),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 11),
                      height: 25,
                      width: 5,
                      color: Colors.yellow,
                    ),
                    
                    SizedBox(
                      width: 10,
                    ),
                    NormalText(text: "ການບໍລິການ",),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Container(
                  height: 1,
                  width: double.infinity,
                  margin: EdgeInsets.only(left: 20, right: 20),
                  color: Colors.white,
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 30, right: 30),
                      child: 
                      BTNIcon(iconbt: Icon(Icons.storefront),
                      onPressed: (() {}),)
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 0, right: 30),
                      child: 
                      BTNIcon(iconbt: Icon(Icons.all_inbox),
                      onPressed: (() {}),)
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 0, right: 30),
                      child: 
                      BTNIcon(iconbt: Icon(Icons.local_shipping),
                      onPressed: (() {}),)
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 0, right: 0),
                      child: 
                      BTNIcon(iconbt: Icon(Icons.local_atm),
                      onPressed: (() {}),)
                    ),
                  ],
                ),
                Row(
                  children: [
                    Container(),
                    Padding(
                      padding: const EdgeInsets.only(left: 45, right: 55),
                      child:Texticon(text: "ສາຂາ",)
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 0, right: 40),
                      child: Texticon(text: "ຝາກເຄື່ອງ",)
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 0, right: 25),
                      child: Texticon(text: "ການຈັດສົ່ງ",)
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 0, right: 0),
                      child: Texticon(text: "ເກັບເງິນປາຍທາງ",)
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 11),
                      height: 25,
                      width: 5,
                      color: Colors.yellow,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    NormalText(text: "ບໍລິການເສີມ",),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Container(
                  height: 1,
                  width: double.infinity,
                  margin: EdgeInsets.only(left: 20, right: 20),
                  color: Colors.white,
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 30, right: 30),
                      child: BTNIcon(iconbt: Icon(Icons.calculate),
                      onPressed: (() {}),)
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 0, right: 30),
                      child: BTNIcon(iconbt: Icon(Icons.settings),
                      onPressed: (() {}),)
                    ),
                  ],
                ),
                Row(
                  children: [
                    Container(),
                    Padding(
                      padding: const EdgeInsets.only(left: 35, right: 35),
                      child: Texticon(text: "ຄິດໄລ່ຄ່າສົ່ງ",)
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 0, right: 43),
                      child:Texticon(text: "ແກ້ໄຂປັນຫາ",)
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildIndicator() => AnimatedSmoothIndicator(
        activeIndex: activeInDex,
        count: imgList.length,
        effect: SlideEffect(
            dotColor: Colors.white,
            dotHeight: 5,
            dotWidth: 5,
            activeDotColor: Color.fromARGB(255, 255, 132, 0)),
      );
}

