import 'package:flutter/material.dart';
import 'package:widgetlifecycle/screen/login_screen/tab_screen/login.dart';
import 'package:widgetlifecycle/screen/login_screen/tab_screen/registor.dart';
import 'package:widgetlifecycle/screen/login_screen/wiget/login_layout.dart';


class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const DefaultTabController(
      length: 2,
      child: Scaffold(
        body: LoginLayout(
          childrenLogin: TabLogin(),
          childrenRegistor: Tapregistor(),
        ),
      ),
    );
  }
}