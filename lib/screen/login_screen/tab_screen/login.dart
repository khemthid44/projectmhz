import 'package:flutter/material.dart';

import 'package:widgetlifecycle/screen/login_screen/wiget/login_button.dart';
import 'package:widgetlifecycle/screen/login_screen/wiget/login_input.dart';
import 'package:widgetlifecycle/screen/login_screen/wiget/login_textbutton.dart';
import 'package:widgetlifecycle/screen/login_screen/wiget/login_title.dart';
import 'package:widgetlifecycle/screen/main_screen.dart';


class TabLogin extends StatelessWidget {
  const TabLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _mobileNo = TextEditingController();
    final _password = TextEditingController();
    
    return Flexible(
      child: Column(
        children: [
          const LoginTitle(text: "ເບີໂທລະສັບ"),
          LoginInput(controller: _mobileNo, hint: "020"),
          const LoginTitle(text: "ລະຫັດຜ່ານ"),
          LoginInput(controller: _password, hint: "***********",),
          LoginTextButton(text: "ລືມລະຫັດຜ່ານ?", onPressed: () {}),
          LonginButton(
            text: "ລົງຊື່ເຂົ້າໃຊ້",
            onPressed: () {
              
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return MainScreen();

              }));
            },
          ),
        ],
      ),
    );
  }
}