import 'package:flutter/material.dart';
import 'package:widgetlifecycle/models/acc.dart';

import 'package:widgetlifecycle/screen/login_screen/wiget/login_input.dart';
import 'package:widgetlifecycle/screen/login_screen/wiget/login_title.dart';
import 'package:widgetlifecycle/screen/login_screen/wiget/registor_button.dart';


class Tapregistor extends StatelessWidget {
  const Tapregistor({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _mobileNo = TextEditingController();
    final _password = TextEditingController();
    final confirm_password = TextEditingController();
    account accounts = account();
    return Flexible(
      child: Column(
        children: [
          const LoginTitle(text: "ເບີໂທລະສັບ"),
          LoginInput(controller: _mobileNo, hint: "020"),
          const LoginTitle(text: "ລະຫັດຜ່ານ"),
          LoginInput(controller: _password, hint: "***********"),
          const LoginTitle(text: "ຢືນຢັນລະຫັດຜ່ານ") ,
          LoginInput(controller: confirm_password, hint: "***********"),
          Container(
            child: RegistorButton(
            text: "ລົງທະບຽນ ",
            onPressed: () {
              print(_mobileNo.text);
              print(_password.text);
            },
          ),
          )
          
          
          
        ],
      ),
    );
  }
}