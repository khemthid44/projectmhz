import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginLayout extends StatelessWidget {
  final Widget? childrenLogin, childrenRegistor;
  const LoginLayout({
    Key? key,
    this.childrenLogin,
    this.childrenRegistor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("image/bg.webp"),
          fit: BoxFit.fill,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 150),
        child: Container(
          height: 700,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15), topRight: Radius.circular(15)),
            color: Color.fromARGB(255, 255, 255, 255),
          ),
          child: Column(
            children: [ 
                
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Container(
                  // color: Colors.red,
                  alignment: Alignment.topCenter,
                  child: 
                  Image.asset("image/mhzLOGO.png",),
                  width: MediaQuery.of(context).size.width - 170, height: 100,

                  // width: 300,
                  // height: 150,
                ),
              ),
              
              SizedBox(
                height: 0,
              ),
              Container(
                
                margin: EdgeInsets.only(left: 15,right: 15,top: 0,bottom: 15),
                alignment: Alignment.topCenter,
                child: TabBar(
                    indicatorColor: Colors.orange,
                    labelColor: Color.fromARGB(
                        255, 255, 140, 25), //<-- selected text color
                    unselectedLabelColor: Color.fromARGB(255, 0, 0, 0),
                    tabs: [
                      Tab(
                        child: Text(
                          "ເຂົ້າສູ່ລະບົບ",
                          style: GoogleFonts.notoSansLao(),
                        ),
                      ),
                      Tab(
                        child: Text(
                          "ລົງທະບຽນ",
                          style: GoogleFonts.notoSansLao(),
                        ),
                      ),
                    ]),
              ),
              Expanded(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: TabBarView(
                    
                    children: <Widget>[
                      SingleChildScrollView(child: childrenLogin!),
                      SingleChildScrollView(child: childrenRegistor!),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}