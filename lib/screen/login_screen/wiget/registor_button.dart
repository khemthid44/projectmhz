import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class RegistorButton extends StatelessWidget {
  final String text;
  final Function() onPressed;
  
  const RegistorButton({
    Key? key,
    required this.text,
    required this.onPressed,
    
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 120,right: 120,bottom: 0,top: 90),
      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
      child: MaterialButton(
        minWidth: 300,
        onPressed: onPressed,
        color: Color.fromARGB(255, 200, 198, 198),
        splashColor: Color.fromARGB(255, 247, 120, 2),
        highlightElevation: 1.0,
        child: Text(
          text,
          style: GoogleFonts.notoSansLao(
            textStyle: TextStyle(
              fontWeight: FontWeight.bold,
              color: Color.fromRGBO(255, 254, 254, 1),
            ),
          ),
        ),
      ),
    );
  }
}