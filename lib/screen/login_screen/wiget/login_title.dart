import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginTitle extends StatelessWidget {
  final String text;
  const LoginTitle({
    Key? key,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(1.0),
      padding: const EdgeInsets.only(left: 25.0, right: 25.0),
      alignment: Alignment.bottomLeft,
      child: Text(
        text,
        style: GoogleFonts.notoSansLao(
          color: Color.fromARGB(255, 0, 0, 0),
        ),
      ),
    );
  }
}