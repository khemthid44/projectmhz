import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginTextButton extends StatelessWidget {
  final String text;
  final Function() onPressed;
  const LoginTextButton({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(0),
      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
      child: MaterialButton(
        minWidth: 20,
        onPressed: onPressed,
        highlightElevation: 1.0,
        child: Text(
          text,
          style:
              GoogleFonts.notoSansLao(color: Color.fromARGB(255, 255, 111, 0)),
        ),
      ),
    );
  }
}