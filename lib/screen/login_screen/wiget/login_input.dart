import 'package:flutter/material.dart';

class LoginInput extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  const LoginInput({
    Key? key,
    required this.controller,
    required this.hint,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(15.0),
      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
      height: 40,
      child: Center(
        child: TextField(
          controller: controller,
          style: TextStyle(fontSize: 16),
          //text box
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderSide: BorderSide(
                width: 0,
                style: BorderStyle.none,
              ),
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
            fillColor: Color.fromARGB(77, 194, 194, 194),
            filled: true,
            hintStyle:
                TextStyle(color: Color.fromARGB(77, 31, 31, 31), height: 3),
            hintText: hint,
          ),
        ),
      ),
    );
  }
}