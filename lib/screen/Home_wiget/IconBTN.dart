import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BTNIcon extends StatelessWidget {
  final Icon iconbt;
  final Function() onPressed;
  const BTNIcon({
    Key? key,
    required this.iconbt,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Color.fromARGB(255, 76, 75, 75),
          borderRadius: BorderRadius.circular(45)),
      height: 55,
      width: 55,
      child: IconButton(
        icon: iconbt,
        
        color: Colors.white,
        onPressed: onPressed,
      ),
    );
  }
}