import 'package:flutter/material.dart';

class Mysqure extends StatefulWidget {
  final String title1;
  final String subtitle1;

  const Mysqure({Key? key, required this.title1, required this.subtitle1 }) : super(key: key);
  
  @override
  State<Mysqure> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<Mysqure> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.album, size: 45),
            title: Text('Sonu Nigam'),
            subtitle: Text('Best of Sonu Nigam Song'),
          ),
        ],
      ),
    );
  }
}
