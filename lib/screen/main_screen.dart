import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:widgetlifecycle/screen/History_list.dart';
import 'package:widgetlifecycle/screen/Service_location/location.dart';
import 'package:widgetlifecycle/screen/home.dart';
import 'package:widgetlifecycle/screen/square.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);
  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static  List<Widget> _widgetOptions = <Widget>[
    Myhome(),
    list(),
    MyTheeU(),

    Text(
      'Nothing',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'ບໍລະການ',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list_alt),
              label: 'ປະຫວັດ',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.location_on),
              label: 'ຈຸດບໍລິການ',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.other_houses),
              label: 'ເພີ່ມເຕີມ',
            ),
          ],
          unselectedItemColor: Colors.grey,
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.amber[800],
          onTap: _onItemTapped,
          showUnselectedLabels: true,
          unselectedLabelStyle: GoogleFonts.notoSansLao(color: Colors.grey,fontSize: 12 ),
          selectedLabelStyle: GoogleFonts.notoSansLao(color: Colors.amber[800],fontSize: 12  ),
         
          ),
    );
  }
}
