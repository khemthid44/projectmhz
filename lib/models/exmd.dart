import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Nani {
  String? name;
  String? from;
  String? tell;
  String? lekbin;
  String? sAKHA;
  String? fark;
  Color? colorr;
 

  Nani({this.name, this.from, this.tell, this.lekbin, this.sAKHA, this.fark, this.colorr});

  Nani.fromJson(Map<String, dynamic> json) {
    name = json['Name'];
    from = json['From'];
    tell = json['Tell'];
    lekbin = json['Lekbin'];
    sAKHA = json['SAKHA'];
    fark = json['fark'];
    colorr = json['colorr'];
  
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Name'] = this.name;
    data['From'] = this.from;
    data['Tell'] = this.tell;
    data['Lekbin'] = this.lekbin;
    data['SAKHA'] = this.sAKHA;
    data['fark'] = this.fark;
    data['colorr'] = this.colorr;

    return data;
  }
}